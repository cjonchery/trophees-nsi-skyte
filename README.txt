Pour visualiser le projet, il faut être munis d’un ordinateur windows avec vscode et un ecran FHD si possible

ouvrir le dossier Virtual_env_SKYTE (c'est tres important, sinon les paths ne fonctionnerons pas) avec vscode

Le projet se trouve dans un environnement virtuel,
pour lancer l'environnement virtuel, il suffit de taper dans le terminal du dossier:

>>venv\Scripts\activate

Une petite icône (env) devrait normalement apparaître à côté du chemin du fichier:

>>(env) C:\user\Desktop\Virtual_env_SKYTE

Ensuite, il n’y a plus qu'à lancer le fichier main.py et à cliquer sur le lien qui apparaît dans la console pour visualiser le projet.


Après l utilisation, appuyer sur ctrl+c dans le terminal de main.py pour fermer l’application flask.

entrer la commande suivante pour désactiver l'environnement virtuel:

>>deactivate
