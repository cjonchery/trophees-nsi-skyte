from flask import Flask, render_template, abort, request, redirect, url_for
import csv
import json
from app.api import process_city_data
from app.comptes import creation_de_compte, mdp_correct, delcsv, addcsv
from app.traitement import main_graphiques, main_traitement, conversion_liste_dico, conseils, angles


app = Flask(__name__)
login = False
infos_comptes=[]
emailutilise=""
mdp_incorrect=""


# prend le fichier csv et le transforme en dictionaire python

with open(r'FinalTrophee\static\data\villes.csv', mode='r') as f:
    cities_data = list(csv.DictReader(f,delimiter=','))

@app.route('/')
def index(): 
    """
    retourne la page d'accueil et la possiblité d'acceder a la page des parametres si l'utilisateur est connecté
    """
    if login:
        connexion = '<div class="dropdown"><button class="mainmenubtn">Compte < </button> <div class="dropdown-child"> <a href="/parametres">Compte</a><a href="/deconnexion">Deconnexion</a>'
    else: 
        connexion = '<div class="log"> <button><a href="/connexion">Se Connecter</a></button><button><a href="/creer">Inscription</a></button></div>'

    return render_template("home.html",connexion=connexion)



#données

@app.route('/spots/<code>')
def city_info(code):
    """
    retourne la page des spots avec les données personalisées
    """
    city = None
    for city_data in cities_data:
            if city_data['code'] == code:
                city = city_data
                break
    if city:
        rawArome = str(json.loads(str(process_city_data(city, "arome", ["temp", "precip", "wind", "windGust", "ptype", "lclouds", "mclouds", "hclouds"]))))
        rawgfs = str(json.loads(str(process_city_data(city, "gfsWave", ["waves", "windWaves"]))))
        
        data = main_graphiques(rawArome, rawgfs)
        if login:
           
            danger, aile, planche = main_traitement(rawArome, rawgfs, conversion_liste_dico(infos_comptes))
            couleur = conseils(danger)
            conseil= f"<div class='contenaire-danger'><h2>Conseils</h2><div class='box-danger'><div><h3>Risque calculé: </h3><p>Taille d'aile conseillée: {aile}m²</p><p>Planche conseillée: {planche}</p></div><div class='box {couleur}'>{danger}/5</div></div></div>'"

        else:
            conseil=""

        if login:
            connexion = '<div class="dropdown"><button class="mainmenubtn">Compte < </button> <div class="dropdown-child"> <a href="/parametres">Compte</a><a href="/deconnexion">Deconnexion</a>'
        else: 
            connexion = '<div class="log"> <button><a href="/connexion">Se Connecter</a></button><button><a href="/creer">Inscription</a></button></div>'
        
        
        #Tableau
        
    


        tableau= '<table><tr><td>Heures</td>'
        for i in range (len(data['labels_vent'])):
            tableau += f"<td>{data['labels_vent'][i]}</td>"
        tableau+= '</tr><tr> <td> Vents(k)</td>'
        for i in range (len(data["wind"])):
            tableau+= f'<td>{round(data["wind"][i])}</td>'
        tableau += '</tr><tr> <td>Rafales(k)</td>'
        for i in range (len(data["gust"])):
            tableau+= f"<td>{round(data['gust'][i])}</td>"
        """
        tableau += '</tr><tr> <td>Direction</td>'
        for i in range (len(data["cardinaux"])):
            tableau+= f'<td>{data["cardinaux"][i]}</td>'
        """
        tableau += '</tr><tr> <td>Direction</td>'
        for i in range (len(data['gust'])):
            tableau+= fr'<td><img src="../static\images\arrow.png" style="transform: rotate({data["angles"][i]}deg);"></td>'
        


        tableau+= '</tr></table>'


        return render_template('chart.html', datasets = data, connexion=connexion, ville = city['ville'], conseil = conseil, tableau = tableau) 
    else:
        abort(404)


#Creation de compte

@app.route('/creer')
def creer():
    return render_template('create.html', emailutilise=emailutilise)

@app.route('/checkCreer', methods= ['POST'])
def CheckCreer():
    global emailutilise
    global login
    global infos_comptes

    email = request.form.get("email")
    mdp = request.form.get("mdp")
    if creation_de_compte(email, mdp)== False:
        emailutilise= "<p class='emailutilise'>Email déjà utilisé par un autre compte</p>"
        return redirect(url_for("creer"))
    else:
        emailutilise= ""
        login= True
        infos_comptes= [email, mdp]
        return redirect(url_for('parametres', emailutilise=emailutilise))
    

#Connexion

@app.route('/connexion')
def connexion():
    return render_template('register.html', emailutilise= mdp_incorrect)

@app.route('/checkconnexion', methods=['POST'])
def ckeckconnexion():
    global login
    global infos_comptes
    global mdp_incorrect

    email = request.form.get("email")
    mdp = request.form.get("mdp")
    mdp_check = mdp_correct(email,mdp)
    if mdp_check != False:
        infos_comptes= mdp_check[1]
        mdp_incorrect=""
        login = True
        return redirect(url_for("index"))

    else: 
        mdp_incorrect= "<p class='emailutilise'>Email ou mot de passe incorrect</p>"
        return redirect(url_for("connexion", emailutilise=emailutilise))

#deconnexion

@app.route('/deconnexion')
def deconnexion():
    global login
    global infos_comptes
    login = False
    infos_comptes = []
    return redirect(url_for("index"))

#Parametres

@app.route('/parametres')
def parametres():
    global infos_comptes
    return render_template("user.html", infos_comptes=infos_comptes)

@app.route('/checkparametres', methods=['POST'])
def checkparameters():
    global infos_comptes
    infos_comptes = infos_comptes[:2]
    infos_comptes.append(request.form.get("niveau"))
    infos_comptes.append(request.form.get("poids"))
    for i in range (6,18):
            infos_comptes.append(request.form.get(f'{i}'))
    for i in range (1,4):
            infos_comptes.append(request.form.get(f'board{i}'))
    delcsv(str(infos_comptes[0]))
    addcsv(infos_comptes)
    return redirect(url_for("index"))

#affichages des graphiques





if __name__ == '__main__':
    app.run(debug=True)

