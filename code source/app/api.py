import requests

def process_city_data(city, model, parametres):
    """
    recuperation des données de windy api et a terme comprendra les graphe+analyse et auro son dossier attitré
    """
    data= {
        "lat": city["lat"],
        "lon": city['lon'],
        "model": model,
        "parameters": parametres,
        "key": "4vc6F38D6sxisP3NMdHqKD6UMIYF8sK4"
    }
    
    header = {"Content-Type" :"application/json"}
    s= requests.post("https://api.windy.com/api/point-forecast/v2", json = data, headers = header)
    return s.text