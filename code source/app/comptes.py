#Dictionnaire avec les clés qui sont un nom d'utilisateur ou une adresse mail, et
#les valeurs sont en  premier le mot de passe, en ensuite d'autres informations personnelles si besoin
#Comme par exemple numéro de téléphone, ou autre jsp
#Reset : comptes = {}
import csv
import flask
comptes = {}

def creation_de_compte(adresse,mdp):
    if is_used(adresse) == True:
        #Adresse déja utilisée
        return False
    #c'est bon
    ajout_ligne({'amail': adresse,'mdp': mdp})
    return "Success"



fieldnames = ['amail','mdp']

def ajout_ligne(d):
    with open(r"FinalTrophee\static\data\skyte_util.csv", mode='a') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writerow(d)

#creation_de_compte()

def replace_line_in_csv(line_to_replace:list, new_line:list):
    new_line = line_to_replace + new_line
    with open(r"FinalTrophee\static\data\skyte_util.csv", 'r', newline='') as file:
        reader = csv.reader(file)
        lines = list(reader)

    replaced = False
    for i, line in enumerate(lines):
        if line == line_to_replace:
            lines[i] = new_line
            replaced = True
            break

    if not replaced:
        print("Line to replace not found.")
        return

    with open(r"FinalTrophee\static\data\skyte_util.csv", 'w', newline='') as file:
        writer = csv.writer(file)
        writer.writerows(lines)


def replace_newdata(liste_materiel,nom_util):
    with open(r"FinalTrophee\static\data\skyte_util.csv", mode='r', newline='') as fichier:
        lecteur_csv = csv.reader(fichier)
        for ligne in lecteur_csv:
            if nom_util in ligne:
                replace_line_in_csv(ligne,liste_materiel)

#FORMAT IMPORTANT - ereplace_newdata([le niveau, le poids, les 12 tailles d'ailes, et les 3 planches], en string l'adresse mail)
#- format de la fonction qui rajoute le matériel
# à la ligne "la", dans l'ordre : niveau,poids,aile


def is_used(adresse):
    rows = []
    with open(r"FinalTrophee\static\data\skyte_util.csv", 'r') as file:
        csvreader = csv.reader(file)
        header = next(csvreader)
        for row in csvreader:
            rows.append(row)
        for el in rows:
            if len(el) > 0 and el[0] == adresse:
                return True
        return False
#TESTS : 
#print(is_used('la'))
#ajout_ligne({'amail': 'trucmuche','mdp': 'lalala'})
    
def mdp_correct(addresse, mdp):
    rows = []
    with open(r"FinalTrophee\static\data\skyte_util.csv", 'r') as file:
        csvreader = csv.reader(file)
        header = next(csvreader)
        for row in csvreader:
            rows.append(row)
        for el in rows:
            if len(el) > 0 and el[0] == addresse and el[1]== mdp:
                return True, el
        return False

def delcsv(target):
   with open(r"FinalTrophee\static\data\skyte_util.csv", 'r', newline='') as file:
       reader = csv.reader(file)
       lines = list(reader)
   lines_to_write = [line for line in lines if target not in line]
   with open(r"FinalTrophee\static\data\skyte_util.csv", 'w', newline='') as file:
       writer = csv.writer(file)
       writer.writerows(lines_to_write)


#target c'est un string, c'est l'adresse mail en gros


def addcsv(line:list):
   with open(r"FinalTrophee\static\data\skyte_util.csv", 'a', newline='') as file:
       writer = csv.writer(file)
       writer.writerow(line)


#add_csv_line([0,1,0])
