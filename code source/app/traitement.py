import math
from datetime import datetime, timezone
import ast
# exemple du format de la variable materiel = {"ailes": [0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12], "planches": {"surf": True, "twintip" : True, "foil": False}, "poids": 60, "niveau": 2}

# fonction qui converti le dictionnaire des données recus par le API pour le vent qui va etre utilisé pour faire des graphiques
def angles(dico_data_converti, wind):
    print(dico_data_converti)
    dico_cardinaux= {'N': 90, 'NE': 45, 'E': 0, 'SE':-45, 'S':-90, 'SO':-13, 'NO':135, 'O':180}
    cardinaux = ['N']*len(wind)
    angle= []

    for i in range(len(wind)):
        a = math.acos(dico_data_converti["wind_u-surface"][i]/wind[i])*(180/math.pi)
        if dico_data_converti["wind_v-surface"][i] <= 0:
            angle.append(-a)
        else:
            angle.append(a)

    for i in range(len(wind)):
        for j in dico_cardinaux:
            if j == 'O':
                if abs(dico_cardinaux[cardinaux[i]] - angle[i]) > abs(dico_cardinaux[j] - angle[i]) or abs(-dico_cardinaux[cardinaux[i]] - angle[i]) > abs(dico_cardinaux[j] - angle[i]):
                    cardinaux[i] = j
            else:
                if abs(dico_cardinaux[cardinaux[i]] - angle[i]) > abs(dico_cardinaux[j] - angle[i]):
                    cardinaux[i] = j
    return angle, cardinaux

def convert_dico_graphiques_vent(dico_vent):
    dico_data_converti = ast.literal_eval(dico_vent)
    labels = [datetime.fromtimestamp(el / 1000, timezone.utc).strftime('%H:%M') for el in dico_data_converti["ts"]]
    tab = []
    for i in range(len(dico_data_converti["wind_v-surface"])):
        tab.append(math.sqrt((dico_data_converti["wind_v-surface"][i]*1.9438444924)**2 + (dico_data_converti["wind_u-surface"][i]*1.9438444924)**2))
    wind = tab
    tab = []
    for i in dico_data_converti["gust-surface"]:
        tab.append(i*1.9438444924)
    gust = tab
    tab_angles, cardinaux = angles(dico_data_converti, wind)
    return labels, wind, gust, tab_angles, cardinaux
   


# fonction qui converti le dictionnaire des données recus par le API pour les vagues qui va etre utilisé pour faire des graphiques
def convert_dico_graphiques_vagues(dico_vagues):
    dico_data_converti = ast.literal_eval(dico_vagues)
    if len(dico_data_converti["ts"]) <= 6:
        height = dico_data_converti["waves_height-surface"]
        period = dico_data_converti["waves_period-surface"]
        labels = [datetime.fromtimestamp(el / 1000, timezone.utc).strftime('%H:%M') for el in dico_data_converti["ts"]]
    else:
        height = [dico_data_converti["waves_height-surface"][i]for i in range(6)]
        period = [dico_data_converti["waves_period-surface"][i]for i in range(6)]
        labels = [datetime.fromtimestamp(dico_data_converti["ts"][i] / 1000, timezone.utc).strftime('%H:%M') for i in range(6)]
    return labels, height, period

#Fonction a appeler pour la création des graphiques
def main_graphiques(dico_vent, dico_vagues):

    labels_vent, wind, gust, angles, cardinaux = convert_dico_graphiques_vent(dico_vent)
    labels_vagues, height, period = convert_dico_graphiques_vagues(dico_vagues)
    return {"labels_vent":labels_vent, "wind": wind, "gust": gust, "labels_vagues": labels_vagues, "height":height, "period": period, 'angles': angles, 'cardinaux': cardinaux}

# fonction qui converti le dictionnaire des données recus par le API pour le vent qui va etre utilisé pour faire des calculs et donner les conseils sur le materiel a prendre et le danger 
def converti_dico_data_vent(dico_vent):
    dico_data_converti = ast.literal_eval(dico_vent)
    dico_data_converti["wind_u-surface"] = dico_data_converti["wind_u-surface"][2]*1.9438444924
    dico_data_converti["wind_v-surface"] = dico_data_converti["wind_v-surface"][2]*1.9438444924
    dico_data_converti["gust-surface"] = dico_data_converti["gust-surface"][2]*1.9438444924
    dico_data_converti["temp-surface"] = dico_data_converti["temp-surface"][2] - 273.15
    return  dico_data_converti

# fonction qui converti le dictionnaire des données recus par le API pour les vagues qui va etre utilisé pour faire des calculs et donner les conseils sur le materiel a prendre et le danger 
def converti_dico_data_vagues(dico_vagues):
    dico_data_converti = ast.literal_eval(dico_vagues)
    dico_data_converti["waves_height-surface"] = dico_data_converti["waves_height-surface"][2]
    dico_data_converti["waves_period-surface"] = dico_data_converti["waves_period-surface"][2]
    dico_data_converti["wwaves_height-surface"] = dico_data_converti["wwaves_height-surface"][2]
    return  dico_data_converti

# fonction qui va déterminer un niveau d'intensité des vagues a partir des données converties ce qui permet de simplifier la suite
def func_indice_intensite_vagues(dico_converti_vagues):
    indice = 0
    periode = 20 - dico_converti_vagues["waves_period-surface"]
    hauteur = dico_converti_vagues["waves_height-surface"] 
    clapot = dico_converti_vagues["wwaves_height-surface"]
    indice += (periode/4 + hauteur + clapot*1.5)//1
    return indice

# tres similaire a la fonction func_indice_intensite_vagues() mais pour le vent
def func_indice_intenste_vent(dico_converti_vent):
    indice = 0
    vitesse = math.sqrt(dico_converti_vent["wind_u-surface"]**2 + dico_converti_vent["wind_v-surface"]**2)
    vitesse_max = dico_converti_vent["gust-surface"]
    indice += (vitesse + vitesse_max)//2
    return indice

#utile pour les calculs
def val_absolue(n):
    if n > 0:
        return n
    return -n

#fonction qui a partir des indices calculés précédament et le materiel de l'utilisateur va déterminer le materiel les plus adequt pour les conditions que l'utilisateur possède
def evaluation_materiel(indice_intensite_vent, indice_intensite_vagues, materiel):
    planches = materiel["planches"]
    ailes = materiel["ailes"]
    if indice_intensite_vagues <= 3.5 and indice_intensite_vent <= 15 and planches["foil"]:
        valeur_planche = -1
        planche = "foil"
    elif indice_intensite_vagues <= 7 and planches["twintip"]:
        valeur_planche = 2
        planche = "twintip"
    else:
        valeur_planche = 0
        planche = "surf"
    aile_ideale = (1.8208 * (indice_intensite_vent ** -0.988)) * materiel["poids"] - 0.0054 * (indice_intensite_vent**2) + 0.1939 * indice_intensite_vent + 1.0667 + materiel["niveau"] + valeur_planche/2
    aile_plus_proche = ailes[0]
    equart = aile_plus_proche - aile_ideale
    for i in ailes:
        if val_absolue(i - aile_ideale) < val_absolue(equart):
            aile_plus_proche = i
            equart = i - aile_ideale
    return aile_ideale, aile_plus_proche, equart, planche

#fonction qui determine le niveau de danger (manque de materiel ou/et conditions trop violentes)
def func_danger(equart, dico_converti_vent, indice_intesite_vagues):
    danger_vent = dico_converti_vent["gust-surface"] - math.sqrt(dico_converti_vent["wind_u-surface"]**2 + dico_converti_vent["wind_v-surface"]**2)
    danger = danger_vent/4 + indice_intesite_vagues/5
    if equart > 0:
        danger += equart/2
    danger = danger//1.5
    if danger >=5:
        danger = 5
    return int(danger)

#fonction main qui rassemble toutes les fonctions qui permettent de faire les convertions, calculs et comparaisons pour déterminer le meilleur materiel et le niveau de danger
def main_traitement(dico_data_vent, dico_data_vagues, materiel):
    dico_converti_vent = converti_dico_data_vent(dico_data_vent)
    print(func_indice_intenste_vent(dico_converti_vent))
    dico_converti_vagues = converti_dico_data_vagues(dico_data_vagues)
    aile_ideale, aile_plus_proche, equart, planche = evaluation_materiel(func_indice_intenste_vent(dico_converti_vent), func_indice_intensite_vagues(dico_converti_vagues), materiel)
    print(aile_ideale)
    danger = func_danger(equart, dico_converti_vent , func_indice_intensite_vagues(dico_converti_vagues))
    return danger, aile_plus_proche, planche

#convertit le format des données stockées dans le fichier csv en celles exploitables par les programmes d'exploitation
def conversion_liste_dico(materiel):
    ailes=[]
    for i in range (4,16):
        if materiel[i] is not None and len(materiel[i])!= 0:
            print(i)
            ailes.append(int(materiel[i]))
    planches= {'surf': False, 'twintip':False, 'foil': False}
    for i in range (16,19):

        if materiel[i] is not None and len(materiel[i]) != 0:
            planches[materiel[i]] = True
    return {"ailes": ailes, "planches": planches, "poids":int(materiel[3]), 'niveau': int(materiel[2]) }

def conseils(danger):
    color = 'gray'
    if danger == 1:
        color = 'green'
    elif danger == 2:
        color = 'greenyellow'
    elif danger == 3:
        color = 'orange'
    elif danger == 4:
        color = 'red'
    elif danger == 5:
        color = 'black'
    return color


#angles



